# СПИСКИ. Методи для списків.
# fruits = ["apple", "bananas", "cherry"]
# fruits.append("orange")
# print(fruits)
#
# cars = ["Ford", "BMW", "Volvo"]
# fruits.extend(cars)
# print(fruits)
#
# fruits.insert(1, "apricot")
# print(fruits)
#
# x = fruits.index("apricot")
# print(x)
#
# new_fruits = []
# for idx, list_item in enumerate(fruits, start=1):
#     new_fruits.append(f'{idx}. {list_item}')
# print(new_fruits)

# НАБОРИ
# Методи для наборів
vegetables = {"potato", "carrot", "beet", "cabbage", "zucchini", "pumpkin"}
print(vegetables)

#Додавання елементаів до набору
vegetables.add('squash')
print(vegetables)
vegetables.update([32, True, 25, 123654, "potato"])   # до того ще й прибирає повтори
print(vegetables)

# Видалення елементів з набору
vegetables.remove(True)
vegetables.discard(123654)
#vegetables.clear()
print(vegetables)

# Об'єднання наборів
odds = {1, 3, 5, 7, 9}        # набір
evens = {2, 4, 6, 8, 10}     # набір
primes = {2, 3, 5, 7}         # набір
composites = {4, 6, 8, 9, 10}  # набір


print(odds.union(evens))
print(evens.union(odds))


# Повертає набір, що є перетинанням двох (та більше) наборів
print(odds.intersection(primes))

# Видаляє елементи цього набору, яких нема в інших вказаних наборах
print(odds.intersection_update(primes))

# Повертає чи мають два набори перетинання чи ні.
print(evens.isdisjoint(composites))

# Повертає чи містить інший набір єлементи цього набору (повністю всі елементи, якщо чогось не вистачає буде false)
print(evens.issubset(composites))















## Повернення копії набору
# vegetables = {"potato", "carrot", "beet", "cabbage", "zucchini", "pumpkin"}




