class Row:
    def __init__(self, idx, pizza, n):
        self.idx = idx
        self.pizza = pizza
        self.n = n

    def __str__(self):
        return f'{self.idx:3}. "{self.pizza:10}" : {self.n:3}'
