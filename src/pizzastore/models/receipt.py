import datetime


class Receipt:
    def __init__(self, idx, client, phon, row_list):
        self.idx = idx
        self.client = client
        self.phon = phon
        self.row_list = row_list

    def __str__(self):
        rec = str(datetime.datetime.now()) + '\n'
        rec += str(self.client) + '\n'
        rec += str(self.phon) + '\n'
        for item in self.row_list:
            rec += str(item) + '\n'
        return rec
