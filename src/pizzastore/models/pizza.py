class Pizza:
    def __init__(self, idx, title, ingredients, price):
        self.idx = idx
        self.title = title
        self.ingredients = ingredients
        self.price = price


PIZZA_LIST = [
    Pizza(0, 'Hawaii         ', 'Pineapple, corn, chicken, greenery', 10),
    Pizza(1, 'Bavaria        ', 'Sausages, tomatoes, beacon', 9),
    Pizza(2, 'Four sires     ', "Mozzarella cheese, parmesan cheese, cottage cheese, lemon", 12),
    Pizza(3, 'Fish pizza     ', "Fish fillets, plain flour, tomatoes, lemon", 10),
    Pizza(4, 'Hunting        ', 'Sausages, parmesan cheese, tomatoes, greenery', 12),
    Pizza(5, 'Magnolia       ', 'Mozzarella cheese, tomatoes, lemon, podded peas', 8),
    Pizza(6, 'Honolulu       ', 'Tomatoes, bell pepper, parmesan cheese, greenery', 9),
    Pizza(7, 'Hollywood      ', 'Boiled beef, chicken, sausages, boiled pork, lemon, greenery', 14),
    Pizza(8, 'Peperoni       ', 'Sausages, parmesan cheese, tomatoes, lemon', 7),
    Pizza(9, 'Mozzarella     ', 'Mozzarella cheese, parmesan cheese, podded peas, greenery', 11),
    Pizza(10, 'Pineapple      ', 'Pineapple, corn, boiled pork, tomatoes, lemon, greenery', 9),
]


def print_pizza_list(pizza_list):
    for pizza_item in pizza_list:
        print(f'{pizza_item.idx:3}. "{pizza_item.title:10}" : {pizza_item.ingredients:60} (${pizza_item.price:3})')


class PizzaError(Exception):
    def __init__(self, message):
        self.message = message
