class Client:
    def __init__(self, idx, name, phone):
        self.idx = idx
        self.name = name
        self.phone = phone

    def __str__(self):
        return f'{self.idx:3} {self.name:10} {self.phone:65}'
