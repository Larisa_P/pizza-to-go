from controller.controller import Controller
from models.pizza import PizzaError

print('pizzeria', __name__)


def main():
    c = Controller()
    while True:
        print('0 - Exit')
        print('1 - List of pizza')
        print('2 - Use a filter')
        print('3 - Random order')
        user = input('Action: ')
        match user:
            case '0':
                break
            case '1':
                c.process_case1()
            case '2':
                c.process_case2()
            case '3':
                c.process_case3()
            case _:
                raise PizzaError('Ви ввели некоректне значення!')


if __name__ == '__main__':
    main()
