import datetime
import random

from collections import namedtuple


Pizza = namedtuple('Pizza', ['idx', 'title', 'ingredients', 'price'])
Client = namedtuple('Client', ['idx_client', 'name_client', 'telephone', 'pizza_quantity', 'total_summ'])
Order = namedtuple('Order', ['idx_order', 'name_client', 'telephone', 'pizza_name', 'quantity_pizza', 'price_pizza', 'order_price'])

# декоратор
def decor(func):
    def inner_func(o_list):
        print(datetime.datetime.now())
        func(o_list)
        print('Дякуємо, що ви з нами!')
    return inner_func

# idx, title, ingredients, price
PIZZA_LIST = [
    Pizza(0, 'Hawaii         ', 'Pineapple, corn, chicken, greenery', 10),
    Pizza(1, 'Bavaria        ', 'Sausages, tomatoes, beacon', 9),
    Pizza(2, 'Four sires     ', "Mozzarella cheese, parmesan cheese, cottage cheese, lemon", 12),
    Pizza(3, 'Fish pizza     ', "Fish fillets, plain flour, tomatoes, lemon", 10),
    Pizza(4, 'Hunting        ', 'Sausages, parmesan cheese, tomatoes, greenery', 12),
    Pizza(5, 'Magnolia       ', 'Mozzarella cheese, tomatoes, lemon, podded peas', 8),
    Pizza(6, 'Honolulu       ', 'Tomatoes, bell pepper, parmesan cheese, greenery', 9),
    Pizza(7, 'Hollywood      ', 'Boiled beef, chicken, sausages, boiled pork, lemon, greenery', 14),
    Pizza(8, 'Peperoni       ', 'Sausages, parmesan cheese, tomatoes, lemon', 7),
    Pizza(9, 'Mozzarella     ', 'Mozzarella cheese, parmesan cheese, podded peas, greenery', 11),
    Pizza(10, 'Pineapple      ', 'Pineapple, corn, boiled pork, tomatoes, lemon, greenery', 9),
    Pizza(11, 'Funny tomatoes ', 'Mozzarella cheese, corn, boiled pork, tomatoes, lemon, greenery', 10)
]
# idx_client, name_client, telephone, pizza_quantity, total_summ
CLIENT_LIST = [
    Client(0, 'Tina Turner', '380968741235', 5, 59),
    Client(1, 'Michael Jackson', '380955170789', 4, 86),
    Client(2, 'Eleanor Porter', '380679814969', 3, 52),
    Client(3, 'Jonathan Banks', '380991234570', 7, 91),
    Client(4, 'Aaron Paul', '380972358974', 5, 64),
    Client(5, 'Din Norris', '380925689631', 7, 79)
]
# ids_order, name_client, telephone, pizza_name, quantity_pizza, price_pizza, order_price
ORDER_LIST = [
    Order(0, 'Michael Jackson', '380955170789', 'Hollywood', 1, 14, 14),
    Order(1, 'Tina Turner', '380968741235', 'Peperoni', 2, 7, 14),
    Order(2, 'Jonathan Banks', '380991234570', 'Honolulu', 1, 9, 9),
    Order(3, 'Aaron Paul', '380972358974', 'Pineapple', 2, 9, 18),
    Order(4, 'Din Norris', '380925689631', 'Peperoni', 1, 7, 7)
]

@decor
def print_pizza_list(pizza_list):
    for pizza_item in pizza_list:
        print(f'{pizza_item.idx:3}. "{pizza_item.title:10}" : {pizza_item.ingredients:65} (${pizza_item.price:3})')

def print_pizza_client(client_list):
    for client_item in client_list:
        print(f'{client_item.idx_client:3}. "{client_item.name_client:15}" : {client_item.telephone:12} ({client_item.pizza_quantity:3}) (${client_item.total_summ:5})')

@decor
def print_order_client(order_list):
    for order_item in order_list:
        print(f'{order_item.ids_order:3}., "{order_item.name_client:19}" :  {order_item.telephone:12}  "{order_item.pizza_name:14}" ({order_item.quantity_pizza:3}) (${order_item.price_pizza:3}) (${order_item.order_price:4})')

def inf_organization():
    print('Контакти торгової організації: ''\ngrossescheidegg@com.ua', '\n+38(099)5782356')
    return


def main():

    while True:
        print('0 - Exit')
        print('1 - List of pizza')
        print('2 - Use a filter')
        print('3 - Random order')
        user = input('Action: ')
        match user:
            case '0':
                break
            case '1':
                print_pizza_list(PIZZA_LIST)
                print_pizza_client(CLIENT_LIST)
                print_order_client(ORDER_LIST)
            case '2':
                econom_pizza = []
                premium_pizza = []
                print("Виберіть ціновий варіант: \nECONOM (ціна до $10 включно) - натисніть *\nPREMIUM (ціна вище $10) "
                      "- натисніть **")
                action_user = input('Оберіть варіант: ')
                match action_user:
                    case '*':
                        econom_pizza = [pizza_item for pizza_item in PIZZA_LIST if pizza_item.price <= 10]
                        print_pizza_list(econom_pizza)
                        inf_organization()
                    case '**':
                        premium_pizza = [pizza_item for pizza_item in PIZZA_LIST if pizza_item.price > 10]
                        print_pizza_list(premium_pizza)
                        inf_organization()
                    case _:
                        pass

            case '3':    # Random order
                print('Ваше замовлення: ')
                quantity_str = random.randrange(1, 6)    # змінна, яка відповідає за кількість рядків у чеку (без повторювань)/ ціле число від 1 до 5
                example = [pizza_item for pizza_item in PIZZA_LIST]  # змінна, яка витягує назву піци та інше зі списку PIZZA_LIST
                # print('Ваше замовлення: ')
                total = 0        # загальна сума по чеку
                for pizza_item in random.sample(example, quantity_str):
                    k = random.randint(1, 3)       #кількість повторювань однієї піци в рядку
                    print(f'{pizza_item.idx:3}. "{pizza_item.title:10}" : {pizza_item.ingredients:60} (${pizza_item.price:3})', '*', k)
                    summ = pizza_item.price * k       # вартість по рядку!
                    total += summ            # загальна вартість по чеку
                print('Кількість рядків у чеку: ', quantity_str)
                print('Загальна вартість: $', total)
                inf_organization()

            case _:
                pass


if __name__ == '__main__':
    main()

