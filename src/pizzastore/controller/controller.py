from models.pizza import PIZZA_LIST, print_pizza_list
import random


def inf_organization():
    print('Контакти торгової організації:\n' 'grossescheidegg@com.ua', '\n+38(099)5782356')


class Controller:
    def __init__(self):
        pass

    def process_case1(self):
        for pizza_item in PIZZA_LIST:
            print(f'{pizza_item.idx:3}. "{pizza_item.title:10}" : {pizza_item.ingredients:60} (${pizza_item.price:3})')

    def process_case2(self):
        econom_pizza = [pizza_item for pizza_item in PIZZA_LIST if pizza_item.price <= 10]
        premium_pizza = [pizza_item for pizza_item in PIZZA_LIST if pizza_item.price > 10]

        print("Select a price option:")
        print("ECONOM (price up to and including $10) - press *")
        print("PREMIUM (price over $10) - press **")
        action_user = input('Choose an option: ')
        if action_user == '*':
            print_pizza_list(econom_pizza)
            inf_organization()
        elif action_user == '**':
            print_pizza_list(premium_pizza)
            inf_organization()
        else:
            pass

    def process_case3(self):
        quantity_str = random.randrange(1, 6)
        example = PIZZA_LIST
        total = 0
        for pizza_item in random.sample(example, quantity_str):
            k = random.randint(1, 3)
            print(f'{pizza_item.idx:3}. "{pizza_item.title:10}" : {pizza_item.ingredients:60} (${pizza_item.price:3})',
                  '*',
                  k)
            summ = pizza_item.price * k
            total += summ
        print('Кількість рядків у чеку:', quantity_str)
        print('Загальна вартість: $', total)
        inf_organization()
